FROM alpine:3.18
MAINTAINER Marcio Bigolin <marcio.bigolinn@gmail.com>
LABEL Description="Imagem para compilar projetos no GITLAB"

RUN apk --update add git curl rsync openssh lftp \
          php82 php82-curl php82-json php82-openssl php82-simplexml php82-xml php82-dom php82-phar \
          php82-gd php82-session php82-pdo_pgsql php82-pdo_mysql php82-mbstring  \
          php82-ctype php82-tokenizer php82-xmlwriter php82-sodium php82-phar \
          yarn python3 && rm /var/cache/apk/*

#workaround para rodar o PHP na imagem =/
RUN ln -s /usr/bin/php82 /usr/bin/php


RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer 
RUN yarn global add gulp 
WORKDIR /app
